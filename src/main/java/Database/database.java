/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;
//Singletion pattern

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kittimasak
 */
public class database {

    private static database instance = new database();

    public static database getIntstance() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    private Connection conn;

    private database() {
    }

    public static database getInstance() {
        String dbPath = "./db/store.db";
        try {
            if (instance.conn == null || instance.conn.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Database connection");
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Error:JDBC is not exist");
        } catch (SQLException ex) {
            System.out.println("Error:Database cannot connection");
        }
        return instance;
    }

    public static void close() throws SQLException {
        if (instance.conn != null || !instance.conn.isClosed()) {
            instance.conn.close();
            instance.conn = null;
        }
    }
    public Connection getConnection(){
        return instance.conn;
    }
}
